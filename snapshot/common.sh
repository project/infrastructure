#!/bin/bash

# Run a snapshot phase. See below for how it is called. ${suffix} is the phase
# and used in filenames.
function sanitize {
  # Execute common SQL commands.
  [ -f "snapshot/common${suffix}.sql" ] && sudo mysql -o ${db} < "snapshot/common${suffix}.sql"
  # Execute common SQL commands, but don't exit if they fail.
  [ -f "snapshot/common-force${suffix}.sql" ] && sudo mysql -f -o ${db} < "snapshot/common-force${suffix}.sql"

  # Skip if this sanitization and phase does not exit.
  [ ! -f "snapshot/${sanitization}${suffix}.sql" ] && return
  # Execute SQL for this sanitization and phase.
  sudo mysql -o ${db} < "snapshot/${sanitization}${suffix}.sql"
}

function snapshot {
  # Remove initial '.'
  subdir=$(echo "${suffix}" | sed -e 's/^\.//')

  # Create and save a binary snapshot.
  clean_export
  sudo mariabackup --innobackupex --no-timestamp --databases="${dblist}" /var/sanitize/drupal_export/${subdir}
  sudo mariabackup --innobackupex --apply-log --export "/var/sanitize/drupal_export/${subdir}"
  sudo chown -R bender:bender "/var/sanitize/drupal_export/${subdir}"

  # Create a tarball for each database.
  for db in ${dblist}; do
    source_db="${db}"
    if [ "${db}" = 'drupal_export' ]; then
      db='drupal'
    fi
    cd "/var/sanitize/drupal_export/${subdir}"
    # Save a copy of the schema after the tables have been compressed.
    sudo mysqldump --no-data --opt --single-transaction --quick --max-allowed-packet=256M "${source_db}" > "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}-schema.sql"
    tar --use-compress-program=pigz -cvf "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}-binary.tar.gz" ./${source_db}/{*.ibd,*.cfg}
    sudo chown -R bender:bender "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}-binary.tar.gz"
    ln -sfv "${db}${suffix}-${BUILD_NUMBER}-schema.sql" "/var/dumps/${subdir}/${db}${suffix}-schema-current.sql"
    ln -sfv "${db}${suffix}-${BUILD_NUMBER}-binary.tar.gz" "/var/dumps/${subdir}/${db}${suffix}-binary-current.tar.gz"

    # Remove old schema snapshots.
    old_snapshots=$(ls -t /var/dumps/${subdir}/${db}${suffix}-[0-9]*-schema.sql | tail -n +2)
    if [ -n "${old_snapshots}" ]; then
      rm -v ${old_snapshots}
    fi
    # Remove old binary snapshots
    old_snapshots=$(ls -t /var/dumps/${subdir}/${db}${suffix}-[0-9]*-binary.tar.gz | tail -n +2)
    if [ -n "${old_snapshots}" ]; then
      rm -v ${old_snapshots}
    fi

    # Create old mysqldump snapshots for dev databases. These are used for the
    # docker images used for dev.
    if [ "${subdir}" == 'dev' ]; then
      sudo mysqldump --opt --single-transaction --quick --max-allowed-packet=256M "${source_db}" | pbzip2 -p6 > "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}-in-progress.sql.bz2"
      sudo chown -R bender:bender "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}-in-progress.sql.bz2"
      mv -v "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}-in-progress.sql.bz2" "/var/dumps/${subdir}/${db}${suffix}-${BUILD_NUMBER}.sql.bz2"
      ln -sfv "${db}${suffix}-${BUILD_NUMBER}.sql.bz2" "/var/dumps/${subdir}/${db}${suffix}-current.sql.bz2"
      # Remove old snapshots.
      old_snapshots=$(ls -t /var/dumps/${subdir}/${db}${suffix}-[0-9]*.sql.{bz2,gz} | tail -n +2)
      if [ -n "${old_snapshots}" ]; then
        rm -v ${old_snapshots}
      fi
    fi
    cd "/usr/local/drupal-infrastructure"
  done
}

function clean_export {
  sudo chown -R bender:bender "/var/sanitize/drupal_export/${subdir}"
  find "/var/sanitize/drupal_export/${subdir}" -mindepth 1 -maxdepth 1 -exec rm -rfv {} \+
}

function clear_tmp {
  echo "DROP DATABASE IF EXISTS ${db}; CREATE DATABASE ${db};" | sudo mysql ${db}
}
