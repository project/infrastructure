#!/bin/bash

source snapshot/common.sh

# Exit immediately on uninitialized variable or error, and print each command.
set -uex

dblist="drupal_association drupal_groups drupal_localize"

for db in ${dblist}; do 
  echo "### Sanitizing ${db} ###"
  # Sanitize using the DB name.
  sanitization=${db}

  # Snapshot in stages.
  # A snapshot suitable for dev. We remove all private information.
  suffix=.dev
  sanitize

  echo "### Completed sanitizing ${db} ###"
done

# Snapshot the dev stage databases
suffix=.dev
snapshot
clean_export
