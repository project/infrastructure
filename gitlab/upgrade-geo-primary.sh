#!/bin/bash
set -eux

gitlab-ctl stop sidekiq || true

dnf upgrade -y gitlab-ee

gitlab-ctl start sidekiq

gitlab-rake gitlab:geo:check
