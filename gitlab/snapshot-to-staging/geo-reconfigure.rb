# This reconfigures staging geo via a script
Gitlab::Geo.primary_node.update!(url: "https://git.code-staging.devdrupal.org/", internal_url: "https://git1.code-staging.devdrupal.org/", name: "weevil")
Gitlab::Geo.secondary_nodes.first.update!(url: "https://git2.code-staging.devdrupal.org/", internal_url: "https://git2.code-staging.devdrupal.org/", name: "borer", oauth_application_id: nil)
