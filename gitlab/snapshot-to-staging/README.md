This process pauses replication on a production geo secondary, takes a GitLab backup, and snapshots the instance, so it can be restored on staging.

Replication must be paused for the Postgres dump made by GitLab’s backup. Pausing replication does impact production, it would ideally be done from an instance that is not serving traffic. Time paused is less than 10 minutes.


# Preparation

Locally, the `aws` command should be sset up with the `da` profile. For example, `aws ec2 describe-volumes --profile da` should work.


# Running the scripts

## On the production geo secondary

This is currently `gitlab-aws.drupalsystems.org`

```
sudo /usr/local/drupal-infrastructure/gitlab/snapshot-to-staging/backup-geo-secondary.sh
```

## Locally

Replace `…` with the path to your infrastructure respoitory clone.

```
…/infrastructure/gitlab/snapshot-to-staging/snapshot.sh
```

## On the *staging* geo primary

This is currently `gitlabstg1-aws.drupalsystems.org`

Syncing repositories takes some time, run in `tmux`

```
sudo /usr/local/drupal-infrastructure/gitlab/snapshot-to-staging/staging-restore.sh
```

## Locally

Replace `…` with the path to your infrastructure respoitory clone.

```
…/infrastructure/gitlab/snapshot-to-staging/cleanup.sh
```

This removes the volume and snapshot in AWS.
