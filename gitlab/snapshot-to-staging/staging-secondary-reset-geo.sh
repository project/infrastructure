#!/bin/bash
set -eux

# Get tokens.
. /etc/drupal-infrastructure-staging-restore.sh

# Replicate primary DB.
gitlab-ctl stop geo-logcursor
gitlab-ctl stop gitlab-kas
echo "${STAGING_REPLICATION_PASSWORD}" | gitlab-ctl replicate-geo-database --no-wait --force --skip-backup --slot-name=borer --host=172.31.10.76 --sslmode=verify-ca

# Reset tracking DB.
gitlab-ctl stop geo-logcursor
gitlab-ctl stop
gitlab-ctl start postgresql
gitlab-ctl start geo-postgresql
gitlab-rake db:drop:geo DISABLE_DATABASE_ENVIRONMENT_CHECK=1
gitlab-ctl reconfigure
gitlab-ctl start redis
gitlab-rake db:migrate:geo

# Restart.
gitlab-ctl start
gitlab-ctl geo-replication-resume
gitlab-rake gitlab:geo:check

# Remove backups from replicate-geo-database.
rm -rf /var/opt/gitlab/postgresql/data.*
