#!/bin/bash
set -eux

# Get tokens.
. /etc/drupal-infrastructure-staging-restore.sh

export AWS_DEFAULT_REGION=us-west-2

# Find latest snapshot.
snapshot=$(aws ec2 describe-snapshots --filters Name=tag:environment,Values=gitlab-staging-pipeline 'Name=description,Values=GitLab snapshot for staging' Name=status,Values=completed | jq '.Snapshots[] | .StartTime+"\t"+.SnapshotId' --raw-output | sort | tail -n1 | cut -f 2)

# Create volume.
volume=$(aws ec2 create-volume --snapshot-id "${snapshot}" --availability-zone us-west-2c --volume-type gp3 --tag-specifications 'ResourceType=volume,Tags=[{Key=environment,Value=gitlab-staging-pipeline}]' | jq '.VolumeId' --raw-output)
echo "Created volume ${volume}"
aws ec2 wait volume-available --volume-ids "${volume}"

# Attach volume.
aws ec2 attach-volume --device /dev/sdf --instance-id "$(curl -s http://169.254.169.254/latest/meta-data/instance-id)" --volume-id "${volume}"
aws ec2 wait volume-in-use --volume-ids "${volume}"
sleep 5

# Mount production snapshot.
device=$(lsblk -o NAME,SERIAL --paths --list | grep -A 1 "$(sed -e 's/-//' <<< ${volume})" | tail -n 1)
mount "${device/ /}" /mnt/gitlab-production-snapshot

# Unmount production snapshot on exit.
success=0
function cleanup {
  umount /mnt/gitlab-production-snapshot
  aws ec2 detach-volume --volume-id "${volume}"
  aws ec2 wait volume-available --volume-ids "${volume}"
  sleep 5
  aws ec2 delete-volume --volume-id "${volume}"

  if [ "${success}" -eq 0 ]; then
    # Deactivate integrations, like Mattermost.
    gitlab-psql -c "UPDATE integrations SET active = false WHERE type_new != 'Integrations::CustomIssueTracker'" || true
    gitlab-ctl stop || true
  fi
}
trap cleanup EXIT

# Remove previous backups.
find /var/opt/gitlab/backups -maxdepth 1 -name '*_gitlab_backup.tar' -print0 | xargs --null --no-run-if-empty rm -v

# Copy newest backup.
cp -v $(find /mnt/gitlab-production-snapshot/gitlab/backups -maxdepth 1 -name '*_gitlab_backup.tar' -print0 | tail -z -n1) /var/opt/gitlab/backups
backup_file=$(basename "$(find /var/opt/gitlab/backups -maxdepth 1 -name '*_gitlab_backup.tar' | head -n 1)")
chown -v git "/var/opt/gitlab/backups/${backup_file}"

# Check that versions match.
backup_version=$(sed -e 's/^[_0-9]*_\([.0-9]*\)-[a-z]*_gitlab_backup\.tar$/\1/' <<< "${backup_file}")
gitlab_version=$(head -n1 /opt/gitlab/version-manifest.txt | sed -e 's/.* //')
test "${gitlab_version}" = "${backup_version}" && echo "Backup & GitLab versions match!"

# Ensure GitLab is started, and stop processes that access the DB.
gitlab-ctl start
gitlab-ctl stop sidekiq || gitlab-ctl kill sidekiq
gitlab-ctl stop puma
gitlab-ctl status || true

# Restore the backup on staging.
gitlab-rake gitlab:backup:restore force=yes BACKUP="${backup_file%_gitlab_backup.tar}"

# Stop GitLab as we sync repositories.
gitlab-ctl stop || gitlab-ctl stop # gitaly can time out stopping

# Copy repositories.
rsync -az --delete --stats --human-readable /mnt/gitlab-production-snapshot/gitlab/git-data/repositories /var/opt/gitlab/git-data/

# Restart GitLab.
gitlab-ctl start || true # gitlab-kas can time out starting
gitlab-ctl reconfigure

# Reconfigure the geo settings for staging urls.
gitlab-rails runner "eval(File.read '/usr/local/drupal-infrastructure/gitlab/snapshot-to-staging/geo-reconfigure.rb')"
gitlab-ctl stop sidekiq || gitlab-ctl kill sidekiq
gitlab-ctl restart || gitlab-ctl start

gitlab-rake gitlab:elastic:pause_indexing || gitlab-rake gitlab:elastic:pause_indexing

# Set up new token.
# https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token-programmatically
gitlab-rails runner "token = User.find_by_username('drupalbot').personal_access_tokens.create(scopes: ['api', 'read_user', 'sudo', 'read_repository', 'admin_mode'], name: 'staging bot token', expires_at: 30.days.from_now, impersonation: true); token.set_token('${PRIVATE_TOKEN}'); token.save!"

# Deactivate integrations, like Mattermost.
gitlab-psql -c "UPDATE integrations SET active = false WHERE type_new != 'Integrations::CustomIssueTracker'"

# Reconfigure system hooks.
curl -s -g --retry 10 --fail --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://git1.code-staging.devdrupal.org/api/v4/hooks" | tee hooks.txt | jq '.[] | .id' | xargs -I % curl -g --request DELETE --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://git1.code-staging.devdrupal.org/api/v4/hooks/%"
curl -g --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://git1.code-staging.devdrupal.org/api/v4/hooks?url=https://drupal:drupal@www.staging.devdrupal.org/drupalorg-repository-update-event&token=${WEBHOOK_TOKEN}&repository_update_events=true"
curl -g --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://git1.code-staging.devdrupal.org/api/v4/hooks?url=https://drupal:drupal@www.staging.devdrupal.org/drupalorg-issue-fork-merge-request-event&token=${WEBHOOK_TOKEN}&repository_update_events=false&merge_requests_events=true"
curl -g --request PUT --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://git1.code-staging.devdrupal.org/api/v4/application/settings?home_page_url=https://git.code-staging.devdrupal.org/project/drupal&after_sign_out_path=https://git.code-staging.devdrupal.org/"

success=1
