#!/usr/bin/env bash
set -eux

# Run our cleanup function on exit, even on error.
cleanup () {
  gitlab-ctl geo-replication-resume
}

trap cleanup EXIT

# Remove all but one previous backup.
find /var/opt/gitlab/backups -maxdepth 1 -name '*_gitlab_backup.tar' -print0 | head -z -n-1 | xargs --null --no-run-if-empty rm -v

# Take GitLab backup of secondary.
gitlab-ctl geo-replication-pause
gitlab-rake gitlab:backup:create SKIP=repositories,registry,artifacts,remote
gitlab-ctl geo-replication-resume

sync

export AWS_DEFAULT_REGION=us-west-2

# Create snapshot.
own_volume=$(aws ec2 describe-volumes --filters "Name=attachment.instance-id,Values=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)" "Name=attachment.device,Values=/dev/sdh" | jq '.Volumes[0].VolumeId' --raw-output)
snapshot=$(aws ec2 create-snapshot --volume-id "${own_volume}" --description 'GitLab snapshot for staging' --tag-specifications 'ResourceType=snapshot,Tags=[{Key=environment,Value=gitlab-staging-pipeline}]' | jq '.SnapshotId' --raw-output)
echo "Created snapshot ${snapshot}"
