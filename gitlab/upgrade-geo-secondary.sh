#!/bin/bash
set -eux

gitlab-ctl stop sidekiq || true

# Based on https://docs.gitlab.com/ee/administration/geo/replication/upgrading_the_geo_sites.html
# The Jenkins pipeline runs gitlab-ctl geo-replication-pause on each secondary before upgrading primary.

dnf upgrade -y gitlab-ee

gitlab-ctl geo-replication-resume || gitlab-ctl geo-replication-resume
gitlab-ctl restart sidekiq
gitlab-ctl restart puma

gitlab-rake gitlab:geo:check
