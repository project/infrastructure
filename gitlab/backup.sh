#!/bin/bash

DATE=`date +"%Y-%m-%d"`

cleanup () {
  umount /mnt/gitlab_snap
  lvremove --force /dev/gitlab1-vg/gitlab_backup_$DATE
}

#Run our cleanup function on exit, even on error
trap cleanup EXIT

#Create LVM Snapshot with 100G of extents for diff
lvcreate --extents 50%FREE -s -n gitlab_backup_$DATE /dev/gitlab1-vg/root || exit 1

#Create mountpoint if it does not exist and mount snapshot
mkdir -pv /mnt/gitlab_snap || exit 1
mount /dev/gitlab1-vg/gitlab_backup_$DATE /mnt/gitlab_snap || exit 1

#Take Borg backup of create backup tarball and repositories
/usr/bin/ionice -c3 /home/drupalbackup/drupalbackup /mnt/gitlab_snap/var/opt/gitlab || exit 1
