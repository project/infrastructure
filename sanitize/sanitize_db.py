#!/bin/env python
from MySQLdb import *
from schemalists.base import schemalist
import password
import table_customizations
from optparse import OptionParser
import field_formatter
#from multiprocessing import Process, Queue, current_process, freeze_support
import multiprocessing
#import datetime
#from multiprocessing import Pool
#from multiprocessing.dummy import Pool as ThreadPool 
import random
import sys


parser = OptionParser()
parser.add_option('-d', '--dest-db', dest="destdb", help="The name of the database we insert into.")
parser.add_option('-s', '--src-db', dest="sourcedb", help="The name of the database we select from.")
parser.add_option('-p', '--data-profile', dest="dataset", help="The schema list overlay. (boss, redacted, or skeleton)")
(options, args) = parser.parse_args()
if options.dataset == 'boss':
    import schemalists.boss
    print("Like a boss.")

if options.dataset == 'redacted':
    import schemalists.skeleton
    print("redacting!!")

if options.dataset == 'skeleton':
    import schemalists.skeleton
    print("Skeleton attack!")


sourcedb = options.sourcedb
if not sourcedb:
    sourcedb = 'drupal_sanitize'
    print("Using default source {0}".format(sourcedb))
destdb = options.destdb
if not destdb:
    destdb = 'drupal_export'
    print("Using default destination {0}".format(destdb))

db = connect(host=password.host, user=password.user, passwd=password.password)
db1 = connect(host=password.host, user=password.user, passwd=password.password)
db2 = connect(host=password.host, user=password.user, passwd=password.password)
db3 = connect(host=password.host, user=password.user, passwd=password.password)
db4 = connect(host=password.host, user=password.user, passwd=password.password)
db5 = connect(host=password.host, user=password.user, passwd=password.password)
db6 = connect(host=password.host, user=password.user, passwd=password.password)
db7 = connect(host=password.host, user=password.user, passwd=password.password)
db8 = connect(host=password.host, user=password.user, passwd=password.password)
db9 = connect(host=password.host, user=password.user, passwd=password.password)
db10 = connect(host=password.host, user=password.user, passwd=password.password)
db11 = connect(host=password.host, user=password.user, passwd=password.password)
db12 = connect(host=password.host, user=password.user, passwd=password.password)
db13 = connect(host=password.host, user=password.user, passwd=password.password)
db14 = connect(host=password.host, user=password.user, passwd=password.password)
db15 = connect(host=password.host, user=password.user, passwd=password.password)
c = db.cursor()
c1 = db1.cursor()
c2 = db2.cursor()
c3 = db3.cursor()
c4 = db4.cursor()
c5 = db5.cursor()
c6 = db6.cursor()
c7 = db7.cursor()
c8 = db8.cursor()
c9 = db9.cursor()
c10 = db10.cursor()
c11 = db11.cursor()
c12 = db12.cursor()
c13 = db13.cursor()
c14 = db14.cursor()
c15 = db15.cursor()

exit = 0


def generate_base_schemalist(table):
    query = "DESCRIBE `{source}`.`{table}`".format(table=table, source=sourcedb)
    c.execute(query)
    column_names = [e[0] for e in c.fetchall()]
    columns2 = ('",\n        "').join(column_names)
    print("""schemalist.add(
    table="{0}",
    columns=[
        "{1}",
    ])
""".format(table, columns2))
    exit_warn()

def exit_warn():
    global exit
    print("Exiting with warnings!")
    exit = 127

def check_schema():
    c.execute('SHOW TABLES IN `{0}`'.format(sourcedb))
    tables = [e[0] for e in c.fetchall()]
    for table in tables:
        if schemalist.table("_ignore:" + table):
            print("Table '{table}' ignored".format(table=table))
            continue
        if schemalist.table("_nodata:" + table):
            print("Table '{table}' ignored".format(table=table))
            continue
        if not schemalist.table(table):
            print("Table '{table}' not present, base config is:\n".format(table=table))
            generate_base_schemalist(table)
            continue
        query = "DESCRIBE `{source}`.`{table}`".format(table=table, source=sourcedb)
        c.execute(query)
        known_columns = schemalist.known_columns(table)
        unknown_columns = [e[0] for e in c.fetchall() if e[0] not in known_columns]
        if unknown_columns:
            print("Unknown column(s) {columns} found in table '{table}' base config is below. This needs to be merged with with current configuration.\n".format(columns=unknown_columns, table=table))
            generate_base_schemalist(table)

def run():
    c.execute('DROP DATABASE IF EXISTS `{0}`'.format(destdb))
    c.execute('CREATE DATABASE `{0}`'.format(destdb))
    check_schema()

    for table in schemalist.get_tables():
        query = "CREATE TABLE `{dest}`.`{table}` LIKE `{source}`.`{table}`".format(table=table, dest=destdb, source=sourcedb)
        print(query)
        c.execute(query)

    field_handler = field_formatter.Field_Handler()

    qq = multiprocessing.Queue()
    NUMBER_OF_PROCESSES = 6

    for i in range(NUMBER_OF_PROCESSES):
        multiprocessing.Process(target=qrun, args=(qq,i)).start()

    for table in schemalist.get_tables():
        column_names = schemalist.process(table)
        if not column_names:
            #skip data for this table
            continue
        handler = table_customizations.get_handler(table, sourcedb, destdb, field_handler)
        try:
            handler.dataset = options.dataset
        except AttributeError:
            pass
        query = handler.get_sql(column_names)
        qq.put(query)

    # Stop all child processes
    for i in range(NUMBER_OF_PROCESSES):
        qq.put('STOP')

def qrun(qq, i):
    for q in iter(qq.get, 'STOP'):
        if i == 1:
            d1(q)
        elif i == 2:
            d2(q)
        elif i == 3:
            d3(q)
        elif i == 4:
            d4(q)
        elif i == 5:
            d5(q)
        elif i == 6:
            d6(q)
        elif i == 7:
            d7(q)
        elif i == 8:
            d8(q)
        elif i == 9:
            d9(q)
        elif i == 10:
            d10(q)
        elif i == 11:
            d11(q)
        elif i == 12:
            d12(q)
        elif i == 13:
            d13(q)
        elif i == 14:
            d14(q)
        elif i == 15:
            d15(q)
        else:
            d(q)
        print("CPU Core %s has completed a job" % q)

def d(q):
    c.execute(q)
    db.commit()
    c.fetchall()

def d1(q):
    c1.execute(q)
    db1.commit()
    c1.fetchall()

def d2(q):
    c2.execute(q)
    db2.commit()
    c2.fetchall()

def d3(q):
    c3.execute(q)
    db3.commit()
    c3.fetchall()

def d4(q):
    c4.execute(q)
    db4.commit()
    c4.fetchall()

def d5(q):
    c5.execute(q)
    db5.commit()
    c5.fetchall()

def d6(q):
    c6.execute(q)
    db6.commit()
    c6.fetchall()

def d7(q):
    c7.execute(q)
    db7.commit()
    c7.fetchall()

def d8(q):
    c8.execute(q)
    db8.commit()
    c8.fetchall()

def d9(q):
    c9.execute(q)
    db9.commit()
    c9.fetchall()

def d10(q):
    c10.execute(q)
    db10.commit()
    c10.fetchall()

def d11(q):
    c11.execute(q)
    db11.commit()
    c11.fetchall()

def d12(q):
    c12.execute(q)
    db12.commit()
    c12.fetchall()

def d13(q):
    c13.execute(q)
    db13.commit()
    c13.fetchall()

def d14(q):
    c14.execute(q)
    db14.commit()
    c14.fetchall()

def d15(q):
    c15.execute(q)
    db15.commit()
    c15.fetchall()

if __name__ == "__main__":
    multiprocessing.freeze_support()
    run()
    c.close()
    sys.exit(exit)
