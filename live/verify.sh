# Include common live script.
. live/common.sh 'verify'

# Collect information.
cd ${webroot}
git fetch
export version=$(git rev-parse --short HEAD)
export version_available=$(git log "HEAD..origin/$(git rev-parse --abbrev-ref HEAD)" --oneline)
export repo_status=$(git status --short)
export repo_diff=$(git diff)
cd ${WORKSPACE}
export projects=$(${drush} pm-list --status=enabled --pipe)
export features=$(COLUMNS=1000 ${drush} features-list | sed -ne 's/\s*Enabled.*Overridden\s*$//p' | sed -e 's/^.*\s\s//')

# Set up report area.
[ ! -d 'html' ] && git clone 'https://bitbucket.org/drupalorg-infrastructure/site-status-assets.git' 'html'
# Generate HTML report.
php -d display_errors=1 /usr/local/drupal-infrastructure/live/verify-template.php > 'html/index.html'

# Exit with error if there are changes.
[ ! -n "${repo_status}" ]
