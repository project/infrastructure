#!/usr/bin/env bash

# Include common live script.
. live/common.sh 'deploy'

# Update code.
uri="${uri}" branch="${branch:=}" fab -r /usr/local/drupal-infrastructure/live/fabfile.py deploy
cd "${webroot}"

if [ "${updatedb-}" = "true" ]; then
  ${drush} -v updatedb --interactive --cache-clear=0
fi
if [ "${cc_menu-}" = "true" ]; then
  ${drush} -v cc "menu"
fi
if [ "${cc_theme-}" = "true" ]; then
  ${drush} -v cc "theme-registry"
fi
if [ "${cc_cssjs-}" = "true" ]; then
  if [ "${uri}" = "drupal.org" ]; then
    ${drush} -v advagg-cron
  else
    ${drush} -v cc "css-js"
  fi
fi
if [ "${cc_views-}" = "true" ]; then
  ${drush} -v cc "views"
fi
if [ "${cc_block-}" = "true" ]; then
  ${drush} -v cc "block"
fi
if [ "${cc_all-}" = "true" ]; then
  ${drush} -v cc "all"
fi
