import os
from fabric import ThreadingGroup
from invoke import task

# Hosts for production
pool = ThreadingGroup('btch1.drupal.bak', 'www8.drupal.bak', 'www9.drupal.bak', 'www10.drupal.bak')

# Set clone path based on deploy.sh uri
clone_path = f"/var/www/{os.environ['uri']}/htdocs"
branch = os.environ['branch']

@task
def deploy(c):
    pool.run(f"git -C {clone_path!r} fetch --prune", echo=True)
    if branch:
        with settings(warn_only=True):
            pool.run(f"git -C {clone_path!r} branch -D {branch!r}", echo=True)
        pool.run(f"git -C {clone_path!r} fetch origin {branch!r}", echo=True)
        pool.run(f"git -C {clone_path!r} checkout {branch!r}", echo=True)
        pool.run(f"git -C {clone_path!r} branch --set-upstream-to=origin/{branch!r} {branch!r}", echo=True)
    pool.run(f"git -C {clone_path!r} pull", echo=True)
