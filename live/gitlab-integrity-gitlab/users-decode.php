<?php

$stdin = fopen('php://stdin', 'r');
while (($line = fgetcsv($stdin, 0, "\t")) !== FALSE) {
  $line[6] = urldecode($line[6]);
  print implode("\t", $line) . PHP_EOL;
}
