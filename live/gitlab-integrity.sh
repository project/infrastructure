# Exit immediately on uninitialized variable or error, and print each command.
set -uex

mkdir -p www
mkdir -p gitlab

# Start GitLab in the background.
ssh gitlab1-aws.drupalsystems.org /usr/local/drupal-infrastructure/live/gitlab-integrity-gitlab/manifest.sh &

# Users.
echo "SELECT u.git_username, vgu.gitlab_user_id, /*u.name,*/ lower(u.mail), concat(u.git_username, '@', u.uid, '.no-reply.drupal.org') extern_uid, if(u.status AND ur.rid IS NOT NULL, 'active', 'blocked') status, 0, concat('https://www.drupal.org/', coalesce(ua.alias, concat('user/', u.uid))) website, coalesce(substring_index(fm.uri, '/', -1), '') avatar, '_private' commit_email FROM users u INNER JOIN versioncontrol_gitlab_users vgu ON vgu.uid = u.uid LEFT JOIN users_roles ur ON ur.uid = u.uid AND ur.rid = 20 LEFT JOIN url_alias ua ON ua.source = concat('user/', u.uid) LEFT JOIN file_managed fm ON fm.fid = u.picture" | drush -r /var/www/drupal.org/htdocs sql-cli --extra='--skip-column-names' | LC_ALL=C sort > www/users.tsv

# Projects.
echo "SELECT vr.name repository, vgr.gitlab_project_id, lower(vr.name) name, concat('For more information about this repository, visit the project page at https://www.drupal.org/', ua.alias) description, if(fdf_pt.field_project_type_value = 'sandbox', 'sandbox', 'project') namespace, 'f' request_access_enabled, 't' active, 20 merge_requests_access_level, if(fdf_pt.field_project_type_value = 'sandbox', 0, 20) builds_access_level, if(fdf_pt.field_project_type_value = 'sandbox', 0, 20) wiki_access_level, 0 monitor_access_level, 0 environments_access_level, 0 feature_flags_access_level, 0 infrastructure_access_level, 0 releases_access_level FROM versioncontrol_repositories vr INNER JOIN versioncontrol_project_projects vpp ON vpp.repo_id = vr.repo_id LEFT JOIN field_data_field_project_type fdf_pt ON fdf_pt.entity_id = vpp.nid INNER JOIN node n ON n.nid = vpp.nid LEFT JOIN versioncontrol_gitlab_repositories vgr ON vgr.repo_id = vr.repo_id LEFT JOIN url_alias ua ON ua.source = concat('node/', n.nid)" | drush -r /var/www/drupal.org/htdocs sql-cli --extra='--skip-column-names' | LC_ALL=C sort > www/projects.tsv

# Maintainers.
echo "SELECT vr.name repository, u.git_username, 't' FROM versioncontrol_auth_account vaa INNER JOIN users u ON u.uid = vaa.uid AND git_consent = 1 AND git_username IS NOT NULL INNER JOIN versioncontrol_repositories vr ON vr.repo_id = vaa.repo_id INNER JOIN versioncontrol_project_projects vpp ON vpp.repo_id = vr.repo_id INNER JOIN node n ON n.nid = vpp.nid WHERE vaa.access != 0" | drush -r /var/www/drupal.org/htdocs sql-cli --extra='--skip-column-names' | LC_ALL=C sort > www/maintainers.tsv

# Issue forks.
echo "SELECT dif.name repository, dif.gitlab_project_id, lower(dif.name) name, concat('/', dif.nid) description, 'f' request_access_enabled, 't' active, 'f' remove_source_branch_after_merge, 20 builds_access_level, 0 snippets_access_level, 0 wiki_access_level, 0 monitor_access_level, 0 environments_access_level, 0 feature_flags_access_level, 0 infrastructure_access_level, 0 releases_access_level, 2 squash_option FROM drupalorg_issue_forks dif" | drush -r /var/www/drupal.org/htdocs sql-cli --extra='--skip-column-names' | LC_ALL=C sort > www/issue-forks.tsv

# Issue fork maintainers.
echo "SELECT dif.name repository, u.git_username, 30 FROM drupalorg_issue_fork_maintainers difm INNER JOIN users u ON u.uid = difm.uid AND git_consent = 1 AND git_username IS NOT NULL INNER JOIN drupalorg_issue_forks dif ON dif.gitlab_project_id = difm.gitlab_project_id WHERE difm.current_member = 1" | drush -r /var/www/drupal.org/htdocs sql-cli --extra='--skip-column-names' | LC_ALL=C sort > www/issue-fork-maintainers.tsv

# Get results from GitLab.
wait
scp gitlab1-aws.drupalsystems.org:{users,projects,maintainers,issue-forks,issue-fork-maintainers}.tsv gitlab/

code=0

for f in {users,projects,maintainers,issue-forks}; do
  diff -u "www/${f}.tsv" "gitlab/${f}.tsv" | grep '^[+-]' > "${f}.diff" && code=1 || true
done

# Alert if any are non-empty.
exit "${code}"
