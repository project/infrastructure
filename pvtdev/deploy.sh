#!/bin/bash
# Create a development environment for a given "name" on wwwpvtdev1/dbpvtdev1

# Include common dev script.
. pvtdev/common.sh
umask 002

# Usage: write_template "template" "path/to/destination"
function write_template {
  sed -e "s/DB_NAME/${db_name}/g;s/NAME/${name}/g;s/SITE/${site}/g;s/DB_PASS/${db_pass}/g" "pvtdev/${1}" > "${2}"
}

# Fail early if comment is omitted.
[ -z "${COMMENT-}" ] && echo "Comment is required." && exit 1

# Handle drupal.org vs. sub-domains properly
if [ ${site} == "drupal" ]; then
  fqdn="drupal.org"
else
  # Strip any _ and following characters from ${site}, and add .drupal.org.
  # Such as 'qa_7' -> 'qa.drupal.org'
  fqdn="$(echo "${site}" | sed -e 's/_.*//').drupal.org"
fi

export TERM=dumb
drush="drush -r ${web_path}/htdocs -y"
declare -A db_names=( ["events"]="events" ["jobs"]="drupal_jobs" ["association"]="drupal_association" ["security"]="drupal_security")
db_pass=$(pwgen -s 16 1)

[ -e "${web_path}" ] && echo "Project webroot already exists!" && exit 1

# Create the webroot and add comment file
mkdir "${web_path}"
mkdir -p "${web_path}/xhprof/htdocs"
sudo chown -R bender:developers "${web_path}"
echo "${COMMENT}" > "${web_path}/comment"

# Create the vhost config
write_template "vhost.conf.template" "${vhost_path}"

# Clone make file.
if [ "${site}" == "association" ]; then
  git clone ${branch/#/--branch } "git@bitbucket.org:drupalorg-infrastructure/assoc.drupal.org.git" "${web_path}/make"
  make_file="${web_path}/make/assoc.drupal.org.make"
else
  git clone ${branch/#/--branch } "git@bitbucket.org:drupalorg-infrastructure/${fqdn}.git" "${web_path}/make"
  make_file="${web_path}/make/${fqdn}.make"
fi

# Append dev-specific overrides.
if [ "${site}" != "groups" -a "${site}" != "qa" ]; then
  curl 'https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-dev.make' >> "${make_file}"
fi

# Run drush make.
drush make --no-cache --cache-duration-releasexml=300 "${make_file}" "${web_path}/htdocs" --working-copy --no-gitinfofile --concurrency=4

if [ -f "${web_path}/htdocs/sites/all/themes/bluecheese/Gemfile" ]; then
  # Compile bluecheese Sass.
  pushd "${web_path}/htdocs/sites/all/themes/bluecheese"
  bundle install
  bundle exec compass compile
  popd
fi

# Copy static files.
[ -f "${web_path}/make/.gitignore" ] && cp "${web_path}/make/.gitignore" "${web_path}/htdocs/"  # Replace core's file
if [ -d "${web_path}/make/static-files" ]; then
  pushd "${web_path}/make/static-files"
  find . -not -type d -print0 | cpio -pdmuv -0 "${web_path}/htdocs"
  popd
fi

# If Composer Manager module is present, run Composer.
if [ -d "${web_path}/htdocs/sites/default/composer" ]; then
  composer --working-dir="${web_path}/htdocs/sites/default/composer" install
fi

# Add settings.local.php
write_template "settings.local.php.template" "${web_path}/htdocs/sites/default/settings.local.php"

# Add .user.ini PHP settings
write_template "user.ini.template" "${web_path}/htdocs/.user.ini"
write_template "user.ini.template" "${web_path}/xhprof/htdocs/.user.ini"

# Strongarm the permissions
echo "Forcing proper permissions on ${web_path}"
sudo find "${web_path}" -type d -exec chmod g+rwx {} +
sudo find "${web_path}" -type f -exec chmod g+rw {} +
sudo chgrp -R developers "${web_path}"

# Add traces directory after global chown
mkdir -p "${web_path}/xhprof/traces"
sudo chown -R drupal_site:www-data "${web_path}/xhprof/traces"

# Add temporary files and devel mail directories after global chown.
mkdir -p "${web_path}/files-tmp"
sudo chown -R drupal_site:developers "${web_path}/files-tmp"
mkdir -p "${web_path}/devel-mail"
sudo chown -R drupal_site:developers "${web_path}/devel-mail"

# Configure the database and load the binary database snapshot
mysql -e "CREATE DATABASE ${db_name};"
mysql -e "GRANT ALL ON ${db_name}.* TO '${db_name}'@'wwwpvtdev1.drupal.bak' IDENTIFIED BY '${db_pass}';"
ssh dbpvtdev1.drupal.bak sudo /usr/local/drupal-infrastructure/pvtdev/snapshot_to_pvtdev.sh ${db_names[${site}]} ${db_name}

# Run extra SQL.
echo "${extra_sql}" | ${drush} sql-cli

# Run any pending updates.
${drush} -v updatedb --interactive

# Link up the files directory
drupal_files="${web_path}/htdocs/$(${drush} status | sed -ne 's/^ *File directory path *: *\([^ ]*\).*$/\1/p')"
[ -d ${drupal_files} ] && rm -rf ${drupal_files}
ln -s /media/nfs/${fqdn} ${drupal_files}

# Disable modules that don't work well in development (yet)
${drush} pm-disable paranoia
${drush} pm-disable beanstalkd

# Sync xhprof webapp directory
# TODO uncomment if we get this working again.
# rsync -av /usr/share/doc/php5-xhprof/ "${web_path}/xhprof/htdocs/"

# Reload apache with new vhost
restart_apache

# Get ready for development
${drush} vset cache 0
${drush} vdel preprocess_css
${drush} vdel preprocess_js
${drush} pm-enable devel
${drush} pm-enable views_ui
${drush} vset devel_xhprof_directory "/var/www/dev/${name}-${site}.private.devdrupal.org/xhprof/htdocs"
${drush} vset devel_xhprof_url "https://xhprof-${name}-${site}.private.devdrupal.org/xhprof_html"
${drush} vset mailchimp_api_key nope
${drush} vset mailchimp_api_classname MailChimpTest

# Set up test user
${drush} upwd bacon --password=bacon || true

# Prime any big caches
curl --insecure --retry 3 --retry-delay 10 "https://drupal:drupal@${name}-${site}.private.devdrupal.org" > /dev/null
