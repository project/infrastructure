#!/usr/bin/env bash

# Exit immediately on uninitialized variable or error, and print each command.
set -uex

# Current working directory should be the TUF repository, with the 'metadata'
# directory.
#
# If a file has changed, purge it.

purged=0
while read -r line; do
  purged=1
  curl -i -X PURGE "${service_url}${line}" -H "Fastly-Key: ${fastly_token}" -H "Accept: application/json"
done < <(md5sum --check --quiet 'checksums.md5' | sed -e 's/: [A-Z]*//')

if [ ${purged} -eq 1 ]; then
  find 'metadata' -type f -exec md5sum {} + > 'checksums.md5'
fi
