@asia2016
Feature: Registed trademark in footer
  In order to show trademark is present
  As an anonymous user, I should see "registered trademark"

  Scenario: Find registered trademark
    Given I am on "asia2016"
    Then I should see "registered trademark"
